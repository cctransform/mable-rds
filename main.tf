

provider "aws" {
  region = "${var.aws_region}"
}

terraform {
  required_version = "~> 0.11"

  backend "s3" {}
}

data "terraform_remote_state" "mable-test" {
  backend = "s3"

  config {
    region     = "${var.aws_region}"
    bucket     = "${var.state_bucket}"
    key        = "${var.state_file}"
    #kms_key_id = "${var.kms_key}"
  }
}
