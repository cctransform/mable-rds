# Security groups
resource "aws_security_group" "main_db_access" {
  name        = "${var.rds_instance_identifier}-rds-access"
  description = "Allow access to the database"
  vpc_id      = "${var.rds_vpc_id}"

  tags = "${merge(var.tags, map("Name", format("%s-rds", var.rds_instance_identifier)))}"
}

resource "aws_security_group_rule" "allow_db_access" {
  count = "${length(var.source_cidr) > 0 ? 1 : 0}"
  type  = "ingress"

  from_port   = "${var.database_port}"
  to_port     = "${var.database_port}"
  protocol    = "tcp"
  cidr_blocks = ["${var.source_cidr}"]

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_all_outbound" {
  type = "egress"

  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_db_access_for_sg_1" {
  count = "${length(var.source_sg_1) > 0 ? 1 : 0}"

  type = "ingress"

  from_port                = "${var.database_port}"
  to_port                  = "${var.database_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.source_sg_1}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_db_access_for_sg_2" {
  count = "${length(var.source_sg_2) > 0 ? 1 : 0}"

  type = "ingress"

  from_port                = "${var.database_port}"
  to_port                  = "${var.database_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.source_sg_2}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_db_access_for_sg_3" {
  count = "${length(var.source_sg_3) > 0 ? 1 : 0}"

  type = "ingress"

  from_port                = "${var.database_port}"
  to_port                  = "${var.database_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.source_sg_3}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_db_access_for_sg_4" {
  count = "${length(var.source_sg_4) > 0 ? 1 : 0}"

  type = "ingress"

  from_port                = "${var.database_port}"
  to_port                  = "${var.database_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.source_sg_4}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_db_access_for_sg_5" {
  count = "${length(var.source_sg_5) > 0 ? 1 : 0}"

  type = "ingress"

  from_port                = "${var.database_port}"
  to_port                  = "${var.database_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.source_sg_5}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_outbound_to_sg_1" {
  count = "${length(var.destination_sg_1) > 0 ? 1 : 0}"
  type  = "egress"

  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = "${var.destination_sg_1}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_outbound_to_sg_2" {
  count = "${length(var.destination_sg_2) > 0 ? 1 : 0}"
  type  = "egress"

  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = "${var.destination_sg_2}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_outbound_to_sg_3" {
  count = "${length(var.destination_sg_3) > 0 ? 1 : 0}"
  type  = "egress"

  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = "${var.destination_sg_3}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_outbound_to_sg_4" {
  count = "${length(var.destination_sg_4) > 0 ? 1 : 0}"
  type  = "egress"

  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = "${var.destination_sg_4}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}

resource "aws_security_group_rule" "allow_outbound_to_sg_5" {
  count = "${length(var.destination_sg_5) > 0 ? 1 : 0}"
  type  = "egress"

  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = "${var.destination_sg_5}"

  security_group_id = "${aws_security_group.main_db_access.id}"
}
