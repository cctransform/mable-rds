#
# Module: tf_aws_rds
#

# Output the ID of the RDS instance
output "rds_instance_id" {
  value = "${aws_db_instance.main_rds_instance.id}"
}

# Output database name
output "rds_database_name" {
  value = "${aws_db_instance.main_rds_instance.name}"
}

# Output database master username
output "rds_database_username" {
  value = "${aws_db_instance.main_rds_instance.username}"
}

# Output the address (aka hostname) of the RDS instance
output "rds_instance_address" {
  value = "${aws_db_instance.main_rds_instance.address}"
}

# Output endpoint (hostname:port) of the RDS instance
output "rds_instance_endpoint" {
  value = "${aws_db_instance.main_rds_instance.endpoint}"
}

# Output the ID of the Subnet Group
output "subnet_group_id" {
  value = "${var.rds_subnet_group}"
}

# Output DB security group ID
output "security_group_id" {
  value = "${aws_security_group.main_db_access.id}"
}
