
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allow_major_version_upgrade | Allow major version upgrade | string | `false` | no |
| apply_immediately | Specifies whether any database modifications are applied immediately, or during the next maintenance window | string | `false` | no |
| auto_minor_version_upgrade | Allow automated minor version upgrade | string | `true` | no |
| backup_retention_period | How long will we retain backups | string | `0` | no |
| backup_window | When AWS can run snapshot, can't overlap with maintenance window | string | `22:00-03:00` | no |
| copy_tags_to_snapshot | Copy tags from DB to a snapshot | string | `true` | no |
| database_name | The name of the database to create | string | - | yes |
| database_password |  | string | - | yes |
| database_port |  | string | - | yes |
| database_user | Self-explainatory variables | string | - | yes |
| db_parameter_group | Parameter group, depends on DB engine used | string | - | yes |
| destination_sg_1 | List of security groups used as destination for RDS security group | string | `` | no |
| destination_sg_2 | List of security groups used as destination for RDS security group | string | `` | no |
| destination_sg_3 | List of security groups used as destination for RDS security group | string | `` | no |
| destination_sg_4 | List of security groups used as destination for RDS security group | string | `` | no |
| destination_sg_5 | List of security groups used as destination for RDS security group | string | `` | no |
| final_snapshot_identifier | If provided, final snapshot will be made with the identifier value before deleting DB | string | `` | no |
| maintenance_window | The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi' UTC | string | `Mon:00:00-Mon:03:00` | no |
| publicly_accessible | Determines if database can be publicly available (NOT recommended) | string | `false` | no |
| rds_allocated_storage | The allocated storage in GBs | string | - | yes |
| rds_engine_type | Database engine type | string | - | yes |
| rds_engine_version | Database engine version, depends on engine type | string | - | yes |
| rds_instance_class | Class of RDS instance | string | - | yes |
| rds_instance_identifier | Custom name of the instance | string | - | yes |
| rds_is_multi_az | Set to true on production | string | `false` | no |
| rds_storage_type | One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). | string | `standard` | no |
| rds_subnet_group | RDS subnet group | string | - | yes |
| rds_vpc_id | VPC to connect to, used for a security group | string | - | yes |
| skip_final_snapshot | If true (default), no snapshot will be made before deleting DB | string | `false` | no |
| snapshot_identifier | Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05. | string | `` | no |
| source_cidr | List of subnets VPC subnets used as source for RDS security group | list | `<list>` | no |
| source_sg_1 | List of security groups used as source for RDS security group | string | `` | no |
| source_sg_2 | List of security groups used as source for RDS security group | string | `` | no |
| source_sg_3 | List of security groups used as source for RDS security group | string | `` | no |
| source_sg_4 | List of security groups used as source for RDS security group | string | `` | no |
| source_sg_5 | List of security groups used as source for RDS security group | string | `` | no |
| subnets | List of subnets DB should be available at. It might be one subnet. | list | - | yes |
| tags | A map of tags to add to all resources | string | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| rds_database_name | Output database name |
| rds_database_username | Output database master username |
| rds_instance_address | Output the address (aka hostname) of the RDS instance |
| rds_instance_endpoint | Output endpoint (hostname:port) of the RDS instance |
| rds_instance_id | Output the ID of the RDS instance |
| security_group_id | Output DB security group ID |
| subnet_group_id | Output the ID of the Subnet Group |

