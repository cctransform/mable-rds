aws_region = "ap-southeast-2"

state_bucket = "mable-test"
state_file = "terraform/development/terraform.tfstate"

#rds_storage_type = "gp2"
rds_instance_identifier = "mable"
rds_allocated_storage = 10
#rds_engine_type = "postgres"
rds_instance_class = "db.t2.micro"
rds_engine_version = "10.10.0"
db_parameter_group = "postgres10.0"
database_name = "mable"
#database_port = "5432"
#allow_major_version_upgrade = true
#auto_minor_version_upgrade = true
apply_immediately = true
#maintenance_window = ""
skip_final_snapshot = true
#copy_tags_to_snapshot = true
backup_retention_period = 7

database_user = "dbadmin"
