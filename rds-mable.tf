module "rds" {
  source = "../modules/rds"

  # RDS Instance Inputs
  rds_storage_type        = "${var.rds_storage_type}"
  rds_instance_identifier = "${var.rds_instance_identifier}"
  rds_allocated_storage   = "${var.rds_allocated_storage}"
  rds_engine_type         = "${var.rds_engine_type}"
  rds_instance_class      = "${var.rds_instance_class}"
  rds_engine_version      = "${var.rds_engine_version}"
  rds_is_multi_az         = "${var.rds_is_multi_az}"
  db_parameter_group      = "${var.db_parameter_group}"

  database_name     = "${var.database_name}"
  database_user     = "${var.database_user}"
  database_password = "${var.database_password}"
  database_port     = "${var.database_port}"

  # Upgrades
  allow_major_version_upgrade = "${var.allow_major_version_upgrade}"
  auto_minor_version_upgrade  = "${var.auto_minor_version_upgrade}"

  apply_immediately  = "${var.apply_immediately}"
  maintenance_window = "${var.maintenance_window}"

  # Snapshots and backups
  skip_final_snapshot     = "${var.skip_final_snapshot}"
  copy_tags_to_snapshot   = "${var.copy_tags_to_snapshot}"
  snapshot_identifier     = "${var.snapshot_identifier}"
  backup_window           = "${var.backup_window}"
  backup_retention_period = "${var.backup_retention_period}"

  # DB Subnet Group Inputs
  rds_subnet_group = "${data.terraform_remote_state.mable-test.rds_subnet_group}"
  subnets          = ["${data.terraform_remote_state.mable-test.rds_subnets}"]
  rds_vpc_id       = "${data.terraform_remote_state.mable-test.vpc_id}"


}
